import styled from 'styled-components';

export const LandingNav = styled.nav`
    background-color: wheat;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0.75rem 2rem;
    font-size: 1.25rem;
`;

export const NavUl = styled.ul`
    display: flex;
    justify-content: space-evenly;
    list-style: none;
`;

export const NavLi = styled.li`
    margin-left: 1.5rem;
`;